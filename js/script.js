class Main{
	constructor(){
		this.$currentElem = null;
		this.$activeBg;
		this.checkPoint = true;
		this.$bodyElem = document.querySelector('body');
		this.$ship = document.querySelector('.ship');
		this.$moveContainer = document.querySelector('.container-move');
		this.$bubbleContainer = document.querySelector('.container-bubble');
		this.$closePopUp = document.querySelector('.popupCloseButton');
		this.$popUpblock = document.querySelector('.poup-block');
		this.$blockIceberg = document.querySelectorAll('#animation-run');
		

		this.$ship.addEventListener('click',this.shipOut.bind(this));
		this.$closePopUp.addEventListener('click',this.closePopUp.bind(this));
	}
	shipOut(){
		this.$moveContainer.classList.add('move-center');
		this.$ship.classList.add('shipOut');
		this.$bubbleContainer.classList.add('container-bubble-out');

		for(let key of this.$blockIceberg){
			key.id = '';
		}
		setTimeout(add, 15000);
	 	function add(){
	 		let $popUp = document.querySelector('.poup-block');
	 		$popUp.style.display = 'inline-block';
	 	}
	}
	closePopUp(){
		this.$popUpblock.style.display = 'none';
	}

/*	activePopUp(){
	 	setTimeout(add, 15000);
	 	function add(){
	 		let $popUp = document.querySelector('.poup-block');
	 		$popUp.style.display = 'inline-block';
	 	}
	}*/
}
document.addEventListener('DOMContentLoaded',()=> {
	 	this.main = new Main();
});
